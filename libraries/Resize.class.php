<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResizeLibrary
 *
 * @author Pityu
 */

class ResizeLibrary {
    static $library, $arguments, $filename, $folders, $file, $fullfolder, $fullpath, $cachefile, $cachemodifiedtime;
    static $width, $height, $enhancements;
    
    public function __construct() {
        self::parse();
    }
    
    public static function parse(){
        $enhancements = "bw, w";
        $enhancements = str_replace(" ", "", $enhancements);
        $enhancements = array_filter(explode(",",$enhancements));
        
        self::$library = Resources::$library;
        self::$filename = Resources::$file;
        self::$arguments = explode("/", Resources::getFolders());
        
        // if out of the last two arguments only the last one is numeric the argument is width
        if( isset(self::$arguments[count(self::$arguments)-2]) && is_numeric( self::$arguments[count(self::$arguments)-2] ) ){
            self::$width = self::$arguments[count(self::$arguments)-2];
            unset(self::$arguments[count(self::$arguments)-2]);
            
            self::$height = ( is_numeric(end(self::$arguments)) ) ? array_pop(self::$arguments) : 0;
        }
        // if the last two numeric last is height last-1 is width
        else{
            self::$width = ( is_numeric(end(self::$arguments)) ) ? array_pop(self::$arguments) : 0;
            self::$height = 0;
        }
        
        // if there's anywhere in the url argument with : return it's key, use it for ehancement, and delete from the arguments array
        if(  $checkenh = array_has(self::$arguments, ":") ){
            self::$enhancements = $checkenh["value"];
            unset( self::$arguments[$checkenh["key"]] );
        }
        
        // after these, all that remain is folder
        self::$folders = self::$arguments;
        
        self::$fullfolder = ( is_dir( Builder::$template_path . self::$folders[0]) ) ? Builder::$template_path."/". self::getFolders() : Builder::$root."/". self::getFolders();
        $fullpath = self::$fullfolder ."/". self::getFile();
        self::$fullpath = str_replace("//", "/", $fullpath);
	self::$cachefile = Resources::$cachefile;
	self::$cachemodifiedtime = Resources::$cachemodifiedtime;
    }
    
    public static function build(){
        
        if( fileexists(self::$fullpath) ){
            $types = array("jpg","jpeg");
            $image = self::$fullpath;
            $image = ( in_array(returnit( pathinfo($image), "extension" ), $types) ) ? compress_image(self::$fullpath, self::$cachefile, 60) : $image;
            $image = file_get_contents( $image );

            if( !Router::get("debug") )
                echo $image;
            else{
                $image = data_img(self::$fullpath);
                
                echo "<img width=\"500\" src=\"".$image."\" alt=\"\" /> <br/><br/>";
                self::debug();
            }
                
        }
        else{
            Resources::build("html");
            header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
            echo "<h1>404 Not Found</h1>";
            echo "The page that you have requested could not be found.";
            die;
        }
    }
    
    public static function getEnhancements($how="array"){
        $enhancements = self::$enhancements;
        
        if( $how != "array" ) return $enhancements;
        
        $enhancements = array_filter(explode(":",$enhancements));
        
        return $enhancements;
    }
    
    public static function getFolders($type=null){
        if( !$type )
            return implode("/", array_filter(self::$arguments) );
        else
            return self::$folders;
    }

    public static function getFile(){
        return self::$filename;
    }
    
    public static function getFileInfo($wat){
        $file = self::$fullpath;
        $return = null;
            
        if( fileexists($file) ){
            $finfo = pathinfo($file);
        
            if( in_array($wat, array_keys($finfo)) )
                $return = $finfo[$wat];
            elseif( $wat == "created" )
                $return = filectime($file);
            elseif( $wat == "modified" )
                $return = filemtime($file);
            elseif( $wat == "size" )
                $return = filesize_formatted($file);
        }
        
        return $return;
    }
    
    public static function debug(){
        $arguments = ( Router::getArguments() ) ? Router::getArguments() : array();
        echo "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\"  style=\"width: 450px; border: 1px solid black;\">

            <tr> <td colspan=\"2\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-weight:bold;\"> <a target=\"_blank\" href=\"" . Router::url("url") . "\"> URL </a> </td> </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Path:</td>
                <td height=\"30\" valign=\"middle\">" . Router::url("path") . "</td>
            </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Fullpath:</td>
                <td height=\"30\" valign=\"middle\">" . self::$fullpath . "</td>
            </tr>

            <tr>
                <td height=\"30\" valign=\"middle\">Library:</td>
                <td height=\"30\" valign=\"middle\">" . Resources::getClassName(Resources::getLibrary()) . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Folders:</td>
                <td height=\"30\" valign=\"middle\">" .  self::getFolders() . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Gets:</td>
                <td height=\"30\" valign=\"middle\">" . str_replace( ";", "<br/>", rarray( $_GET ) ) . "</td>
            </tr>
            <tr> <td colspan=\"2\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-weight:bold;\"> Cachee </td> </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">File:</td>
                <td height=\"30\" valign=\"middle\">" .  self::$cachefile . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Modified:</td>
                <td height=\"30\" valign=\"middle\">" .  self::$cachemodifiedtime . "</td>
            </tr>
            <tr> <td colspan=\"2\" height=\"30\" align=\"center\" valign=\"middle\" style=\"font-weight:bold;\"> Image </td> </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">File:</td>
                <td height=\"30\" valign=\"middle\">" .  self::getFile() . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Extension:</td>
                <td height=\"30\" valign=\"middle\">" .  self::getFileInfo("extension") . "</td>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Size:</td>
                <td height=\"30\" valign=\"middle\">" .  self::getFileInfo("size") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Width:</td>
                <td height=\"30\" valign=\"middle\">" . self::$width . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Height:</td>
                <td height=\"30\" valign=\"middle\">" . self::$height . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Enhancements:</td>
                <td height=\"30\" valign=\"middle\">" . self::getEnhancements("string") . "</td>
            </tr>
            </tr>
            <tr>
                <td height=\"30\" valign=\"middle\">Created:</td>
                <td height=\"30\" valign=\"middle\">" .  Resources::getFileInfo("created") . "</td>
            </tr>
            </tr>
        </table>";
    }
}

?>
