<nav class="topbar">
    <div class="container">
        <ul class="topbarl topbarl-social topbarl-right">
            <li>
                <a href="http://facebook.com/drlenkeiromania" target="_blank"><i class="fa fa-facebook"></i>
                </a>
            </li>
            <li>
                <a href="http://twitter.com/drlenkeiromania" target="_blank"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
                <a href="http://youtube.com/user/drlenkeiromania" target="_blank"><i class="fa fa-youtube"></i> </a>
            </li>
            <li>
                <a href="http://plus.google.com/drlenkeiromania" target="_blank"><i class="fa fa-google-plus"></i> </a>
            </li>
        </ul>
    </div>
</nav>