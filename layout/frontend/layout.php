<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta name="google-site-verification" content="QWxdgBktgdgP3iGBOlOPQDBz8gXAmiP9hxx7XCV5R-0" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title><?php Template::title(); ?></title>
        <?php
            Template::style("less");
            Template::style("css");
            Template::script();
        ?>
    </head>
    <body>
    <div id="wrapper">        
        <!-- Header -->
        <?php Layout::extend("header"); ?>
        
        <!-- Content -->
        <?php Layout::extend("content"); ?>
            
        <!-- Footer -->
        <?php Layout::extend("footer"); ?>
    </div>
</body>
</html>