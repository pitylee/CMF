<?php
    $navitems=array(
        "rolunk"=>"Rolunk",
        "shop"=>"Webshop",
        "elerhetoseg"=>"Elerhetoseg"
    );
?>
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-top-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="/">
                <img src="design/images/dr-lenkei-logo.png" height="120" alt="Dr Lenkei Romania" />
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-top-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <?php foreach( $navitems as $link=>$title ) { ?>
                <li>
                    <a class="menu-item" href="/<?php echo $link; ?>/"><?php echo $title; ?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>