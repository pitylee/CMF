<?php

class Query{
    
    static $results = array(), $error, $query, $success = false, $rows_count = 0, $total_rows_count = 0;
    static $con;
            
    public function __construct(){
        self::$con = self::connect();
    }
    
    public static function connect(){
        self::$con = mysql_connect(HOST, USERNAME, PASSWORD) or die("ERROR CONNECTING TO DATABASE!: ".mysql_error());
        mysql_select_db(DATABASE) or die("CANNOT SELECT THE DATABASE! ".mysql_error());
        mysql_query("SET NAMES 'utf8'");
        mysql_query("SET CHARACTER SET 'utf8'");
        mysql_query("SET `time_zone` = '".date('P')."'");
        mysql_query("SET GLOBAL time_zone = '+8:00'");
        
        return self::$con;
    }
    
    public static function disconnect(){
        $close = ( self::$con ) ? mysql_close(self::$con) : false;
        
        return $close;
    }
    
    public static function create($query) {
        if( !self::$con )
            self::$con = self::connect();
        
        self::$results = array();
        self::$query = $query;
        
        if(strpos(strtoupper($query), 'SELECT') !== false)
        {
            $sql_s = mysql_query($query) or self::setSQLError('<b>SQL_ERROR:</b> '.mysql_error());
            
            if(strpos(strtoupper($query), 'SQL_CALC_FOUND_ROWS') !== false )
            {
                $rows = mysql_query("SELECT FOUND_ROWS() AS rows");
                $rw = mysql_fetch_array($rows);
                self::$total_rows_count = (int)$rw['rows'];
            }
            
            if($sql_s)
            {
                if(mysql_num_rows($sql_s) > 1)
                {
                    while($sql_r =  mysql_fetch_array($sql_s))
                    {
                        array_push(self::$results, $sql_r);
                        self::$rows_count++;
                    }
                }
                else if(mysql_num_rows($sql_s) == 1)
                {
                    self::$results = mysql_fetch_array($sql_s);
                    self::$rows_count = 1;
                }
            }
            
        }
        else
        {
            self::$success = mysql_query($query) or self::setSQLError('<b>SQL_ERROR:</b> '.mysql_error());
            if(strpos(strtoupper($query), 'SQL_CALC_FOUND_ROWS') !== false )
            {
                $rows = mysql_query("SELECT FOUND_ROWS() AS rows");
                $rw = mysql_fetch_array($rows);
                self::$total_rows_count = (int)$rw['rows'];
            }
        }
        
    }
    private static function setSQLError($sql_error)
    {
        //sql_error($sql_error.'<br/><b>QUERY:</b> '.self::$query);
        self::$error = $sql_error;
       self::printSQLError(true, true);
    }
    public static function getQuery()
    {
        return self::$query;
    }
    public static function getLastID($table){
        $query = "SELECT MAX(`ID`) FROM `".$table."`";
        $query = Query::create($query);
        $query = returnit(Query::getResults(), 0);
        
        return $query;
    }
    public static function getResults($use_array = false)
    {
        $results = self::$results;
        
        if(self::getTotalRowsCount() > 0 && !empty($results))
        {
            if($use_array)
            {
                if(self::getTotalRowsCount() == 1)
                    $results = array($results);
            }
           
            return $results;
        }
        
        return array();
    }
    public static function success()
    {
        return self::$success;
    }
    public static function getRowsCount()
    {
        return (int)self::$rows_count;
    }
     public static function getTotalRowsCount()
    {
        return (int)self::$total_rows_count == 0 ? (int)self::$rows_count : (int)self::$total_rows_count;
    }
    public static function getSQLError($show_query = false)
    {
        $error = self::$error;
        
        if($show_query && $error != '')
        $error .= '<br/><b>QUERY:</b> '.self::$query;
        
        return $error;
    }
    public static function printSQLError($show_query = false, $exit = false)
    {
        $error_id = 'error_'.rand(999,999).date("Hi");
        $error = self::$error;
        
        if($error != '')
        {
            if($show_query)
            {
                $trace = ''; $debug = debug_backtrace();
                for($i=0;$i<sizeof($debug);$i++)
                {
                    if(!empty($debug[$i]['file']))
                    {   
                        $style = '';
                        if($i == 2 || $i == 3)
                        $style = 'font-weight:bold;';
                        
                        $trace .= '<br/><span style="'.$style.'">'.$debug[$i]['file'].' on line: '.$debug[$i]['line'].' function='.$debug[$i]['function'].'()</span>';
                    }
                }
                        
                $error .= '<br/><b>QUERY:</b> '.self::$query.'<br/><b style="color:red;">TRACE</b>: '.$trace;
            }
            $rand_func_id = rand(999, 9999);
           
            $error_log = '
                <a href="javascript: showError(\''.$error_id.'\');"><span style="color:Red;">ERROR: </span>Show error</a>
                <div id="'.$error_id.'" style="color:red; padding: 5px 5px 5px 5px;display:none;">'.$error.'</div>';
            
            if(!empty($error_log))
                sql_error ($error_log);
            
            if($exit)
                exit($error_log);
            else
                echo $error_log;
        }
    }
}
?>
