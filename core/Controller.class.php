<?php
/**
 * This is our base controller class. Controllers containt all the logic of the
 * application and are reponsible for passing data from models to views.
 */
class Controller
{
    // This is a factory method. It determines a class name of a controller
    // we want to load, includes a file and creates an object for us
    public static function create($name)
    {
        $controller = Builder::checkController($name);
        $classname = self::getClassName($name);
        
        if( $controller  )
        {
            require_once($controller);
            return new $classname;
        }
        else
            return null;
    }

    public static function getClassName($name)
    {
        $name = str_replace("/", "", $name);
        $classname = ucfirst($name) . 'Controller';
        return $classname;
    }

    public static function controllerExists($name)
    {
        $controller = Builder::checkController($name);
        $exists = ( fileexists($controller) ) ? true : false;
        
        return $exists;
    }

    // We use this method to convert an action name
    // to a method name in a controller class
    public static function getActionName($name)
    {
        return $name . 'Action';
    }
}

?>