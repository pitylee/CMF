<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Router
 *
 * Depends: Router, library called
 * @author Pityu
 */
class Resources {
        
    static $param, $path, $chunks, $library, $file, $extension, $folders, $current, $template, $folder;
    static $cachefile, $cachemodifiedtime;
    
    public function __construct($path=null) {
        self::$param = Router::url("param");
        self::$path = ( $path ) ? $path : self::$param;
        
        self::$path = (self::$path) ? self::$path : "";
        self::$chunks = explode('/', self::$path);
        self::$chunks = array_values( array_filter(self::$chunks) );
        self::$folder = (self::$chunks[0] == "uploads" || cache_skip( self::getFile() ) && cache_skip( self::$path ) ) ? Builder::$root . "/" : Builder::$template_path;
    }
     
    public static function parse()
    {
        self::$library = array_shift(self::$chunks);
        self::$file = array_pop(self::$chunks);
        self::$folders = ( count(self::$chunks) > 0 ) ? self::$chunks : array("");

        self::$extension = (self::$library == "less") ? "css" : self::getFileInfo("extension");
        
        self::$template = Template::parse();
    }
        
    public static function getLibrary(){
        return self::$library;
    }
    
    public static function getFolders($type=null){
        
        if( !$type )
            return implode("/", self::$folders) . "/";
        else
            return self::$folders;
    }
    
    public static function getFile(){
        $template = Template::parse();
        $jquery_version = $template["javascript"][0]["jQuery_version"];
        
        if( self::$file == "jquery.js" )
            return "jquery-" . $jquery_version . ".js";
        else
            return self::$file;
    }
    
    public static function getFileInfo($wat=null){
        $file = self::$folder . self::getLibrary() ."/". self::getFolders() . self::getFile();
        $return = null;
        if( fileexists($file) ){
            $finfo = pathinfo($file);
            
            if( !$wat ) return $finfo;
                    
            if( in_array($wat, array_keys($finfo)) )
                $return = $finfo[$wat];
            elseif( $wat == "created" )
                $return = filectime($file);
            elseif( $wat == "modified" )
                $return = filemtime($file);
        }
        
        return $return;
    }

    public static function init(){
        // We parse the url so we can use it for determine the parts of it
        self::parse();
	self::$cachefile = Builder::$cache_path . self::$library ."/". self::getFolders() . self::$file;
	self::$cachemodifiedtime = ( cache_modtime(self::$cachefile) ) ? time() - cache_modtime(self::$cachefile) : time();
        
        $library = self::getLibrary();
        $file = self::getFile();
        $folders = self::getFolders();

        $libraryClassName = self::getClassName($library);
        $libraryClass = self::create($library);
        
        // We create gets from the url like it was real
        Router::buildGets();
        
        // If we find a controller and the method, we call it
        if( class_exists($libraryClassName) ){
            if( method_exists($libraryClass,"build") )
                $libraryClass->build($library, $file, $folders);
        }
    }
    
    public static function create($name=null)
    {
        $name = ($name) ? $name : self::getLibrary();
        $library = Builder::checkLibrary($name);
        $classname = self::getClassName($name);
        
        if( fileexists($library) && class_in_file($classname, $library) )
        {
            self::build(self::$extension);
            
            return new $classname;
        }
        else{
            $file = self::$folder . self::getLibrary() . self::getFolders() . self::getFile();
            if( fileexists($file) ){
                self::build(self::$extension);
                $contents = file_get_contents($file);
                echo $contents;
            }
            else{
                // 404
            }
        }
    }

    public static function build($type){
        $mime = Builder::mime($type);
        
        if( !Router::get("debug") ){
            Header("Content-Type: " . $mime );
            header ("cache-control: must-revalidate");
        }
    }
    
    public static function getClassName($name=null)
    {
        $name = ($name) ? $name : self::getLibrary();
        $name = str_replace("/", "", $name);
        $classname = ucfirst($name) . "Library";
        return $classname;
    }
}

?>
