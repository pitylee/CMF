<?php
/**
 * This is our base controller class. Controllers containt all the logic of the
 * application and are reponsible for passing data from models to views.
 */
class Model
{
    // This is a factory method. It determines a class name of a controller
    // we want to load, includes a file and creates an object for us
    public static function create($name)
    {
        $classname = self::getClassName($name);
        $model = Builder::checkModel($name);
        
        if( $model  )
        {
            require_once($model);
            return new $classname;
        }
    }

    public static function getClassName($name)
    {
        $name = str_replace("/", "", $name);
        $classname = ucfirst($name) . "Model";
        return $classname;
    }

    // We use this method to convert an action name
    // to a method name in a controller class
    public static function getActionName($name)
    {
        return $name . 'Action';
    }
}

?>