<?php

/**
 * This is our base view class.
 * Views are responsible for presenting data to the user.
 * In this example, we're using raw PHP as a templating system.
 */
class View
{
    static $current, $view_path, $extend_path;
    
    public  function __construct() {
        self::$current = "layout";
    }
    
    public static function extend($layout)
    {
        $layout = strtolower("layout/". $layout);
        self::$extend_path = Builder::checkView($layout);
        self::$current = $layout;
        
        if( fileexists(self::$extend_path) )
            Core::inject(self::$extend_path);
    }
    
    public static function render()
    {   
        self::$view_path = Builder::checkView("view");
        
        if( fileexists(self::$view_path) )
            Core::inject(self::$view_path);
    }
    
    public static function norender(){ return false; }
            
}

?>