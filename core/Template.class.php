<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Template
 *
 * @author Pityu
 */
class Template {
    static $current, $template_info, $resurces_uri;
    static $cachefile, $cachemodifiedtime;
    static $title, $pattern;
    static $fonts, $jquery;
    
    public function __construct() {
        if( !Router::get("debug") )
            error_reporting(0);
        
        self::$template_info = self::parse();
        self::$current = self::current();
        self::$jquery = false;
        Layout::prepend( self::parsefonts(), "head");
        Layout::prepend( Template::preload(true), "head");
    }
    
    public static function current(){
        self::$current = Core::getSettings("site_template");
        
        // self::$current = "default";
        
        return self::$current;
    }
    
    public static function init(){
        
    }
    
    public static function parse(){
        $template_json = Builder::$template_path . "template.json";
        $template = ( fileexists($template_json) ) ? json_decode( file_get_contents($template_json), true ) : array();
        
        foreach( returnit( $template["variables"], 0) as $variable => $ovalue ){
            // Replace colors
            if( is_hex($ovalue) ){
                if( $ovalue[0] != "#" ){
                    $ovalue = "#" . $ovalue;
                }
            }
            
            // Replace variables values   
            if( has($ovalue, "@") ){
                preg_match_all("/@([A-Za-z0-9_-]+)/", $ovalue, $matches);
                
                // preg_match_all("/@([\w]([A-Za-z0-9_-]+))[^\w]/", $ovalue, $matches);
                if( $matches[0] ){
                    // if multiple vars in line
                    if( count($matches[0]) > 1 ){
                        
                    // Here's the magic, so that we can have multiple vars!
                        foreach( $matches[0] as $k=>$v ){
                            $var = str_replace("@", "", $matches[0][$k]);
                            $val = $matches[1][0];

                            $toval = ( isset( $template["variables"][$var] ) ) ? $template["variables"][$var] : "";

                            $ovalue = str_replace("@".$var, $toval, $ovalue);
                        }
                    }
                    //if only one 
                    else{
                        $var = str_replace("@", "", $matches[0][0]);
                        $val = $matches[1][0];

                        $toval = ( isset( $template["variables"][0][$var] ) ) ? $template["variables"][0][$var] : "";

                        $ovalue = str_replace("@".$var, $toval, $ovalue);

                    }
                }
            }
            $template["variables"][0][$variable] = $ovalue;
        }
        
        if( !is_array($template) ) return;

        return $template;
    }
    
    public static function parsefonts(){
        self::$fonts = ( isset( self::$template_info["google_fonts"] ) ) ? self::$template_info["google_fonts"][0] : array();
        
        foreach( self::$fonts as $font=>$style){
            $font = ( strlen($font) > 3 && !is_int($font) ) ? $font : "";
            $style = ( $style && strlen($style) > 3 ) ? $style : "";
            
            
            if( !$font ) $fontpack = $style;
            elseif( !$style ) $fontpack = $font;
            else $fontpack = $font.":".$style;
            
            $prependfont .= self::font($fontpack, true);
        }
        return $prependfont."\n";
    }
    
    public static function title($arg=null){
        error_reporting(E_ALL);
        $print=true;
        
        $delimiter = "%s";
        if( is_array($arg) ){
            $pattern = $arg["pattern"];
            
            self::$pattern = $pattern;
            $arg=null;
            $print=false;
        }
        else{
            if( !self::$pattern ){
                self::$pattern = Core::getSettings("SITE_TITLE_PATTERN");
            }
        }
        
        $argument = ( $arg ) ? true : false;
        $arguments = array();
        $arguments[] = Core::getSettings("SITE_TITLE");
                        
        if( $argument ){
            $print=false;
            $arg = str_replace("pattern", "", $arg);
            $arg = str_replace("|", ",", $arg) . ",";
            $argument = str_replace(" ,", ",", $arg);
            $argument = explode(",", $argument);
            $argument = array_filter($argument);

            foreach($argument as $key=>$value ){
                $arguments[$value] = $value;
            }
        
            $argcount = count($arguments);
            $splitcount = substr_count(self::$pattern, $delimiter);
            $argdifference = $splitcount - $argcount;
        
            if( $argdifference > 0 ){
                $patternsplit = explode($delimiter, self::$pattern);
                array_splice($patternsplit, 0, 1);
                
                $temp_pattern = "";
                foreach( $patternsplit as $key=>$pat ){
                
                    if( $key < $argcount-1 ){
                        $temp_pattern .= $delimiter . $pat;
                    }
                    if( $key == $argcount-1 ){
                        $temp_pattern .= $delimiter;
                    }
                }
                
                // self::$pattern .= $delimiter;
                self::$pattern = $temp_pattern;
            }
            /*
            echo "<br/>new pattern: " . self::$pattern . "<br/><br/>";
            */
            
            // dump( preg_match('/[%\d\$]+/',$m) );
            
            self::$title = printf_array( self::$pattern, $arguments );
            
        }
        
        if( !self::$title ){
            self::$title = printf_array( $delimiter, $arguments );
        }
        
        if( $print )
            echo self::$title;
    }
    
    public static function meta($type, $content=null){
        
    }
    
    public static function script($src="js", $content=null){
        echo "\n";
        self::parseJs($src, $content);
    }
    
    public static function style($src="css", $content=null){
        echo "\n";
        self::parseCss($src, $content);
    }
    
    public static function uri($cachefile=null){
        $pathinfo = pathinfo($cachefile);
        
        $dirname = ( isset($pathinfo["dirname"]) ) ? $pathinfo["dirname"] : ""; 
        $extension = ( isset($pathinfo["extension"]) ) ? $pathinfo["extension"] : ""; 
        
        $filename = $pathinfo["filename"];
        $filename = str_replace(".min" , "", $filename);
        $folder = ($cachefile && has($pathinfo["dirname"],"/")) ? explodeit("/", $dirname, 1) : $dirname;
	$cachefile = ($cachefile) ? Builder::$cache_path . $cachefile : null;
        
        $WillSkip = ( $folder == "less" || cache_skip($filename) || cache_skip($folder)  ) ? "no" : "yes";
        // every single css which is not style will be cached only once.
        $WillSkip = ( $extension == "css" && $folder != "less" ) ? "yes" : $WillSkip;
        $WillSkip = ( !Core::isDev() ) ? "yes" : $WillSkip;
        
        if( $cachefile && $WillSkip == "yes" && cache_exists($cachefile) )
            $uri = Builder::$cache_uri;
        else
            $uri = Builder::$resources_uri;
        
        return $uri;
    }
    
    private static function parseCss($src, $content){
        if( Router::isBot() ) return;
        
        $url = self::uri() . $src . "/";
        if( $src == "less" ){
            $scriptfull = "less/style.css";
            $template_uri = self::uri($scriptfull);
            $file_path = $template_uri . $scriptfull;
            echo "<link rel=\"stylesheet\" href=\"".$file_path."\" />\n";
        }
        elseif( $src == "css" )
        {
            $css_folder = Builder::$template_path . "css/*.css";
            $css_files = glob($css_folder);
                       
            foreach( $css_files as $css ){
                $filename = returnit( pathinfo($css), "basename");
                $scriptfull = "css/" . $filename;
                $template_uri = self::uri($scriptfull);
                $file_path = $template_uri . $scriptfull;
                
                // also skip the files with starting with cache_skip defined
                $maybeskip = ( $filename ) ? explodeit(".", $filename, 0) : null;
                
                if( cache_skip($maybeskip) ) return;
            
                $cssurl = $url . $filename;
                echo "<link rel=\"stylesheet\" href=\"".$file_path."\" />\n";
            }
        }
        elseif( $content ){
            $content = LessLibrary::minify($content);
            echo "<style type=\"text/css\">\n\t".$content."\n</style>\n";
        }
        else{
            $remotehost = returnit( parse_url($src), "host" );
            $pathinfo = ( pathinfo($src) );
            $cachefile = fixpath( $pathinfo["basename"] );
            
            
            if( has($src, "?family=") ){
                $font = explodeit("?family=", $src, 1);
                $font = explodeit(":", $font, 0);
                $font = str_replace("+", "_", $font);
                $font = strtolower($font);
                
                $cachefile = $cachefile . "_" . $font;
            }
            
            $cachepath = Builder::$cache_path . "downloaded/".$remotehost."/".$cachefile .".css";
            $cacheuri = Builder::$cache_uri . "downloaded/".$remotehost."/".$cachefile .".css";
            
            if( !fileexists( $cachepath ) ){
                $headers = http_response($src);
                $content_type = $headers["content_type"];;

                if( is_url($src) && $content_type == Builder::mime("css") ){
                    $file_path = Builder::download($src, $cachepath, "Less::fixpaths");
                }
            }
            
            $file_path = $cacheuri;
                
            
            echo "<link rel=\"stylesheet\" href=\"".$file_path."\" />\n";
        }
        
    }
    
    public static function font($fontname=null, $return=false){
        $src = "http://fonts.googleapis.com/css?family=" . $fontname;
        $remotehost = returnit( parse_url($src), "host" );
        $pathinfo = ( pathinfo($src) );
        $cachefile = fixpath( $pathinfo["basename"] );

        if( has($src, "?family=") ){
            $font = explodeit("?family=", $src, 1);
            $font = explodeit(":", $font, 0);
            $font = str_replace("+", "_", $font);
            $font = strtolower($font);

            $cachefile = $cachefile . "_" . $font;
        }

        $cachepath = Builder::$cache_path . "downloaded/".$remotehost."/".$cachefile .".css";
        $cacheuri = Builder::$cache_uri . "downloaded/".$remotehost."/".$cachefile .".css";

        if( !fileexists( $cachepath ) ){
            $headers = http_response($src);
            $content_type = $headers["content_type"];;

            if( is_url($src) && $content_type == Builder::mime("css") ){
                $file_path = Builder::download($src, $cachepath, "Less::fixpaths", $src);
            }
        }
        $file_path = $cacheuri;

        $link = "<link rel=\"stylesheet\" href=\"".$file_path."\" />\n";
        
        if(!$return) echo $link;
        else return $link;
    }
    
    private static function parseJs($src, $content){
        if( Router::isBot() ) return;
        
        //error_reporting(E_ALL);
        $template = self::$template_info;
        if( $src=="js" && $template && isset($template["javascript"]) ){
            $js = $template["javascript"][0];
            $jquery_version = $template["javascript"][0]["jQuery_version"];

            if( isset($js["plugin"]) ){ $javascripts = $js["plugin"]; }
            elseif( isset($js["plugins"]) ){ $javascripts = $js["plugins"]; } 
            else return;

            $javascripts = str_replace(" ", "", $javascripts);
            $javascripts = explode(",", $javascripts);
            $javascripts = array_filter($javascripts);
            $template_path = Builder::$template_path."js/";
            $jquery = $template_path."jquery-".$jquery_version.".js";
            $jquery_settings = $template_path."jquery-settings.js";
            
            $scriptfull = "js/jquery.js";
            $template_uri = self::uri($scriptfull);
            $file_path = $template_uri . $scriptfull;
            echo "<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
            self::$jquery = true;

            foreach( $javascripts as $script ){
                    $scriptfull = "js/" . $script.  ".js";
                    $template_uri = self::uri($scriptfull);
                    $file_path = $template_uri . $scriptfull;
                    echo "<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
                    
                    $bootstrap_folder = Builder::$template_path . "js/".$script."/*.js";
                    $bootstrap_folder = glob_recursive($bootstrap_folder);
                    
                    foreach( $bootstrap_folder as $subscript ){
                        $filename = returnit( pathinfo($subscript), "filename" );
                        
                        $scriptfull = "js/" . $script."/" .$filename.  ".js";
                        $template_uri = self::uri($scriptfull);
                        $file_path = $template_uri . $scriptfull;
                        echo "<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
                        
                    }
                    
                /*
                if( $script == "bootstrap" ){
                    $bootstrap_files = array("bootstrap");
                    
                    $bootstrap_folder = Builder::$template_path . "js/*\/*.js";
                    $bootstrap_folder = glob($bootstrap_folder);
                   
                    foreach( $bootstrap_folder as $script ){  $bootstrap_files[] = "bootstrap/" . pathinfo($script)["filename"]; }
                    
                    foreach( $bootstrap_files as $script ){
                        $scriptfull = "js/" . $script.  ".js";
                        $template_uri = self::uri($scriptfull);
                        $file_path = $template_uri . $scriptfull;
                        echo "\t<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
                    }
                }
                else{
                    $scriptfull = "js/" . $script.  ".js";
                    $template_uri = self::uri($scriptfull);
                    $file_path = $template_uri . $scriptfull;
                    echo "\t<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
                }
                */
            }

            if( fileexists($jquery_settings) ){
                $scriptfull = "js/jquery-settings.js";
                $template_uri = self::uri($scriptfull);
                $file_path = $template_uri . $scriptfull;
                echo "<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n\n";
            }
        }
        elseif( $content ){
            echo "<script type=\"text/javascript\">\n\t\t".$content."\n\t</script>\n";
        }
        else{
            $remotehost = returnit( parse_url($src), "host");
            $cachefile = returnit( pathinfo($src), "basename");
            $cachepath = Builder::$cache_path . "downloaded/".$remotehost."/".$cachefile;
            $cacheuri = Builder::$cache_uri . "downloaded/".$remotehost."/".$cachefile;
            
            if( !fileexists( $cachepath ) ){
                $headers = http_response($src);
                $content_type = $headers["content_type"];;

                if( is_url($src) && $content_type == Builder::mime("js") || $content_type == Builder::mime("xjs") || $content_type == Builder::mime("ajs")  ){
                    $file_path = Builder::download($src, $cachepath, "Js::minify");
                }
            }
            $file_path = $cacheuri;
            echo "<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
        }
    }
    
    public static function preload($return=false){
        $scriptfull = "js/gen_preload.js";
        $preloader_script = Builder::$template_path .$scriptfull;
        $cachepath = Builder::$cache_path .$scriptfull;
        
        $template_uri = self::uri($scriptfull);
        $file_path = $template_uri . $scriptfull;
        
        if( Core::isDev() || !is_file($preloader_script) ){
            $preload = Preload::collection();
            $allowpreload = ( isset(self::$template_info["allow_preload"]) ) ? self::$template_info["allow_preload"] : "css,img";
            $allowpreload = str_replace(", ", ",", $allowpreload);
            $allowpreload = str_replace(" ,", ",", $allowpreload);
            $allowpreload = explode(",", $allowpreload);
            
            
            /* original, fullpath, basename, filename, extension, resources, rsrc */
            $preload_js = "";
            foreach( $preload as $type=>$preloadsub ){
                if( in_array($type, $allowpreload) ){
                    $preload_js .= "/* ".$type." */\n";
                    $jqArray = $type . "Array";
                    $preload_js .= "var " . $jqArray . " = [];\n";
                    
                    foreach( $preloadsub as $full=>$pre ){
                        $id = ($pre["id"]) ? $pre["id"] : "";
                        $original = $pre["original"];
                        $fullpath = $pre["fullpath"];
                        $basename = $pre["basename"];
                        $filename = $pre["filename"];
                        $extension = $pre["extension"];
                        $resources = $pre["resources"];
                        $rsrc = $pre["rsrc"];

                        if(is_image($fullpath) ){
                            $currentjqElm = array(
                                // "fullpath" => $fullpath,
                                "id" => $id,
                                "original" => $original,
                                "resources" => $resources,
                                "rsrc" => $rsrc
                            );
                            
                            $currentjqElm = json_encode($currentjqElm, JSON_PRETTY_PRINT);
                            $preload_js .= $jqArray.".push(".$currentjqElm.");\n";
                        }
                        
                    }
                    $preload_js .= "\n\n\n";
                }
            }
            
/*
    $($jqArray).each(function(){
        allImgs[k] = new Image(); //new img obj
        allImgs[k].src = baseURL + this.rsrc;
        k++;
    });
 
            $preload_js .= "";
            
            $preload_js .= "console.log(".$jqArray.");";
            $preload_js .= "alert(cssArray[0].resources);";
 */
            writefile($preloader_script, $preload_js);
            writefile($cachepath, Js::minify($preload_js));
        }
        
        
        // dump($preload);
        $script = "<script type=\"text/javascript\" src=\"".$file_path."\"></script>\n";
        
        if(!$return) echo $script;
        else return $script;
    }
}

?>