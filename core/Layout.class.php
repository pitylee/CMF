<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Layout
 *
 * @author Pityu
 */
class Layout{
    static $current, $root, $layout_path, $view_path, $extend_path, $layout;
    static $prepend, $append;
    
    public function __construct(){
        self::$root = $_SERVER["DOCUMENT_ROOT"];
        self::current(Core::getSettings("default_layout"));
        self::$prepend=array();
        self::$append=array();
    }

    public static function build($name=null)
    {
        $name = ($name) ? $name : "layout";
        self::$layout_path = (self::$current == "default" ) ? Core::$root ."/". Core::getSettings("default_layout_path") ."/default/" : Core::$root ."/".Core::getSettings("default_layout_path")."/". self::$current ."/";
        $layout_path = self::$layout_path . $name . ".php";
        
        if( debug == true && Core::isDev() )
            
            // HERE
            self::prepend( "debug" );
        
        if( fileexists($layout_path) ){
            Core::inject($layout_path);
        }
        else
            die("Error: ".$name."/" . self::$current ." layout not found in " . $layout_path);
    }

    public static function prepend( $toprepend, $element="body" ){
        self::$prepend[$element] = ( isset(self::$prepend[$element]) ) ? self::$prepend[$element] . $toprepend : $toprepend;
        $return = self::$prepend;
        
        return $return;
    }
    
    public static function append( $toappend, $element = "body" ){
        self::$append[$element] = ( isset(self::$append[$element]) ) ? self::$append[$element] . $toappend : $toappend;
        $return = self::$append;
        
        return $return;
    }

    public static function init(){
        // We parse the url so we can use it for determine the parts of it
        Router::parse();

        $lang = Router::getLang();
        $controller = Router::getController();
        $action = Router::getAction();
        $arguments = Router::getArguments();

        $controllerClassName = Controller::getClassName($controller);
        $controllerClass = Controller::create($controller);
        $actionName = Controller::getActionName($action);

        $modelClassName = Model::getClassName($controller);
        $modelClass = Model::create($controller);

        // We create gets from the url like it was real
        Router::buildGets();
         
        $isError = preg_match("/^[0-9]{3}(?![0-9])(.shtml|.html|.htm)$/",$controller);

        if( $isError ){
            $error = explode(".", $controller);
            $errorcode = $error[0];
            $status = Builder::statusCode($errorcode);
            
            self::generateError($errorcode);
        }
        
            
        // If we find a controller and the method, we call it
        if( class_exists($controllerClassName) ){
            if( method_exists($controllerClass,"build") )
                $controllerClass->build();

            self::$layout = ( isset($controllerClass::$layout) ) ? $controllerClass::$layout : self::$layout;

            if( method_exists($controllerClass,$actionName) )
                $controllerClass->$actionName($arguments);

            if( class_exists($modelClassName) )
                if( method_exists($modelClass,"build") )
                    $modelClass->build();

        }
        
        new View();
        
        self::build(self::$layout);
    }
    
    public static function generateError($errorcode="500", $message=null){
        $status = Builder::statusCode($errorcode);

        Resources::build("html");
        header($_SERVER["SERVER_PROTOCOL"] . " " .  $status["code"] ." ". $status["message"]);
        echo "<h1>". $status["code"] ." ". $status["message"] ."</h1>";
        echo $message;
        echo "There was a problem with your request.";
        die;      
    }
    
    public static function current($name){
        self::$current = $name;
    }

    public static function extend($extend)
    {   
        // $file = strtolower(self::$layout_path . $layout . ".php");
        self::$extend_path = Builder::checkLayout($extend);
        
        if( fileexists(self::$extend_path) )
            Core::inject(self::$extend_path);
    }
    
    public static function inject(){
        $controller = Router::getController();
        $modelClassName = Model::getClassName($controller);
        $methods = get_class_methods($modelClassName);

        $methods = ( is_array($methods) ) ? array_diff($methods, array("build")) : array();

        $routerstuff=array(
            "path" => Router::$path,
            "chunks" => Router::$chunks,
            "lang" => Router::$lang,
            "controller" => Router::$controller,
            "model" => $modelClassName,
            "model_methods" => $methods,
            "action" => Router::$action,
            "arguments" => Router::$arguments
        );
        
        foreach( $methods as $method ){ $routerstuff[$$method] = $modelClassName::$method(); }

        // We extract all the variables from router's pass function (usually used in controller) and inject into the layout
        $variables = (is_array(Router::$var_to_pass)) ? Router::$var_to_pass : array();
        $variables = array_merge($variables, $routerstuff);
        
        return $variables;
    }

    public static function include_smart($fileName, $append=array(), $prepend=array())
    {
        $variables = self::inject();

        // we get the file contents
        ob_start();
        ob_implicit_flush(false);
        extract($variables);
        include($fileName);
        $phpcontent = ob_get_contents();
        ob_end_clean();
        
        // we replace the appends
        foreach($append as $element=>$toappend){
            $element = "<".$element.">";
            $toappend = $element ."\n". $toappend ."\n";

            $phpcontent = str_replace($element, $toappend, $phpcontent);
        }
        
        // we replace the prepends
        foreach($prepend as $element=>$toprepend){
            $element = "</".$element.">";
            $toprepend = "\n".$toprepend ."\n". $element."\n";

            $phpcontent = str_replace($element, $toprepend, $phpcontent);
        }
        
        $phpcontent = self::parseIMG($phpcontent);
        
        if( !Core::isDev() )
            $phpcontent = self::minimize($phpcontent);
        else
            $phpcontent = str_replace("</script>" , "</script>\n", $phpcontent);
        
        
        if( isset($preload) && is_array($preload) )
            Preload::collection($preload);
        
        
        echo $phpcontent;
    }
    
    public static function minimize($html){
        $html = preg_replace("/<!--.*?-->/ms","",$html);
        $html = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '	', '	'), '', $html);
        
        return $html;
    }
    
    public static function parseIMG($phpcontent){
        $html = new DOMDocument();
        @$html->loadHTML($phpcontent);
        $imageTags = $html->getElementsByTagName("img");

        $jquery = Template::$jquery;
            
        foreach($imageTags as $key=>$img) {
            $originalSrc = $img->getAttribute("src");
            
            if( !has($originalSrc, "rsrc/") && !has($originalSrc, "resources/") ){
                $chunks = explode("/", $originalSrc);
                $src = ( isset($chunks[0]) && $chunks[0] == "design" || isset($chunks[1]) && $chunks[1] == "design") ? str_replace("design", "", $originalSrc) : $originalSrc;
                $isrsrc = false; // later if we want to be rsrc anyway...
                $original = fixpath($src);
                $selfroot = ( isset($chunks[0]) && $chunks[0] == "design" || isset($chunks[1]) && $chunks[1] == "design" ) ? Builder::$template_path : Builder::$root;
                $imgpath = fixpath($selfroot ."/". $original);
                $src = ( has($src, "resize/") ) ? $src : "resize/".$src;
                $cachepath = fixpath(Builder::$cache_path ."/". $src);
                $fullpath = realpath( $imgpath );
                $cachedb = "SELECT * FROM `cache` WHERE `Fullpath`='".$fullpath."'";
                $cachedb = Query::create($cachedb);
                $cachedb = Query::getResults();
                $cachedb = ( isset($cachedb["Original"]) && $cachedb["Original"] == $imgpath ) ? $cachedb : "";
                $cachefile = $src;
                $id = ( isset($cachedb["ID"]) ) ? $cachedb["ID"] : Query::getLastID("cache") + $key +1;
                $id = "preload-img-".$id;
                $pathinfo = pathinfo($fullpath);
                $rsrc = fixpath("/rsrc/".$src);
                $resources = fixpath("/resources/".$src);
                $prettybasename = reverse_prettyurl($pathinfo["filename"]);

                if( $fullpath ){
                    $index = $fullpath;
                    $preload["img"][$index]["id"] = $id;
                    $preload["img"][$index]["original"] = $original;
                    $preload["img"][$index]["resources"] = $resources;
                    $preload["img"][$index]["rsrc"] = $rsrc;
                    $preload["img"][$index]["fullpath"] = $fullpath;
                    $preload["img"][$index]["cachepath"] = $cachepath;
                    $preload["img"][$index]["basename"] = $pathinfo["basename"];
                    $preload["img"][$index]["filename"] = $pathinfo["filename"];
                    $preload["img"][$index]["extension"] = $pathinfo["extension"];

                    $toreplace = array();
                    $newsrc = ( fileexists($cachepath) && has($originalSrc, "uploads") || !Core::isDev() || $isrsrc === true ) ? $rsrc : $resources;

                    // dump( fileexists($cachepath) ); 

                    if( ALLOW_CSS_DATAIMG && is_image($cachepath) && $isrsrc === false )
                        $newsrc = ( fileexists($cachepath) && !Core::isDev() ) ? data_img($cachepath) : $newsrc;

                    // preparing the new attributes
                    $toreplace["src"] = $newsrc;
                    $toreplace["id"] = ( $img->getAttribute("id") ) ? $img->getAttribute("id") : $id;
                    $toreplace["alt"] = ( $img->getAttribute("alt") ) ? $img->getAttribute("alt") : $prettybasename;
                    $toreplace["title"] = ( $img->getAttribute("title") ) ? $img->getAttribute("title") : $prettybasename;


                    if( $jquery ){
                        $toreplace["data-preload-id"] = $id;
                        $toreplace["data-preload-resource"] = ( !fileexists($cachepath) ) ? $resources : $rsrc;
                        $toreplace["data-resource"] = ( !fileexists($cachepath) ) ? $resources : $rsrc;
                    }

                }
                else{
                    // We will give'em rsrc, that will give 404
                    $toreplace["src"] = $rsrc;
                }

                // set the attributes
                foreach( $toreplace as $attr=>$value ){
                    $img->setAttribute($attr, $value);
                }
            }
        }
        
        if( Core::isDev() )
            $html->formatOutput = true;
        
        $phpcontent=$html->saveHTML();        
        
        return $phpcontent;
    }
    

    public static function debug(){
        $arguments = ( Router::getArguments() ) ? Router::getArguments() : array();
        
        $debug = "<div class=\"cmf_debugger\"> <a href=\"#debug\" class=\"btn btn-default debug_toggle\" id=\"debug_toggle\">
            <i class=\"glyphicon glyphicon-filter\"></i> 
        </a>";
        $toggled = ( Router::get("debug") ) ? " toggled" : "";
        $debug .= "<div id=\"cmf_debug\" class=\"scrollbox". $toggled ."\"><div class=\"debug_title\"><span>Debug</span></div>";
        $debug .= "<script type=\"text/javascript\">$(document).ready(function(){ $('#debug_toggle').on('click', function(){ $('#cmf_debug').toggleClass('toggled'); return false; }); });</script>";
        $debug .= "<div class=\"table-responsive\"><table class=\"debug_table table table-striped\">";
         $debug .= "<tr> <td colspan=\"2\" class=\"main_title\"> <a target=\"_blank\" href=\"" . Router::url("url") . "\"> URL </a> </td> </tr>

            <tr>
                <td class=\"title\">Path:</td>
                <td class=\"value\">" . Router::url("path") . "</td>
            </tr>

            <tr>
                <td class=\"title\">Lang:</td>
                <td class=\"value\">" . Router::getLang() . "</td>
            </tr>
            <tr>
                <td class=\"title\">Controller:</td>
                <td class=\"value\">" .  Router::getController() . "</td>
            </tr>
            <tr>
                <td class=\"title\">Action:</td>
                <td class=\"value\">" .  Router::getAction() . "</td>
            </tr>
            <tr>
                <td class=\"title\">Arguments:</td>
                <td class=\"value\">" . str_replace( ",", "<br/>", implode(",", $arguments ) ) . "</td>
            </tr>
            <tr>
                <td class=\"title\">Gets:</td>
                <td class=\"value\">" . str_replace( ";", "<br/>", rarray( $_GET ) ) . "</td>
            </tr>

            <tr> <td colspan=\"2\" class=\"main_title\"> Layout </td> </tr>
            <tr>
                <td class=\"title\">Main:</td>
                <td class=\"value\">" . Layout::$current . "</td>
            </tr>
            <tr>
                <td class=\"title\">View:</td>
                <td class=\"value\">" . View::$current . "</td>
            </tr>
            <tr>
                <td class=\"title\">View path:</td>
                <td class=\"value\">" . Router::$view_path . "</td>
            </tr>";
         
            $debug .= "<tr> <td colspan=\"2\" class=\"main_title\"> User Agent </td> </tr>";
            $userAgent = Router::userAgent();
            unset($userAgent["pattern"]);
            foreach( $userAgent as $name=>$content ){
                $debug .= "<tr>
                    <td class=\"title\">" . ucfirst($name) . "</td>
                    <td class=\"value\">" . $content . "</td>
                </tr>";
            }
        $debug .= "</table></div></div></div>";
        
        return $debug;
    }    
}

?>