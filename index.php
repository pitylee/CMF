<?php  
    $config = $_SERVER['DOCUMENT_ROOT'].'/assets/config.php';
    require_once $config; 
    
    $query = new Query();
    
    $builder = new Builder();
    $router = new Router();
    $layout = new Layout();
    $template = new Template();
    
    Router::setDefaultController( Core::getSettings("site_homepage") );
    Router::setDefaultAction("index");

    // This is the key part, we call a method that has a name stored in $stored
    Layout::init();
    
    ob_end_flush();
    
    Query::disconnect();
?>

